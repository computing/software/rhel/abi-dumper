# abi-dumper

This repo contains the RPM spec file to enable creating binary RPMs for `abi-dumper`.

Taken from the Fedora Packaging repository:
https://src.fedoraproject.org/rpms/abi-dumper
